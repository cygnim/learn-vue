new Vue({
  el: '#exercise',
  data: {
    isHighlighted: {
      highlight: false,
      shrink: true
    },
    test1: "test1",
    test2: "test2",
    custom: "effect",
    barfValue: "",
    isTrue: true,
    custom2back: false,
    percentDone: 100,
    myStyle: {
      backgroundColor: '#f00',
      width: '200px'
    }
  },
  computed: {

    custom2: function(){
      return {
        orange: this.custom2back
      }
    }
  },
  methods: {
    startEffect: function() {
      var vm = this;
      setInterval(function(){
        vm.isHighlighted.highlight = !vm.isHighlighted.highlight;
        vm.isHighlighted.shrink = !vm.isHighlighted.shrink;
      }, 1000)
    },
    barf: function(event) {
      console.log('barf');
      this.custom2back = event.target.value === 'true'
    },
    startProgress: function() {
      var vm = this;
      setInterval(function(){
        vm.percentDone += 100;
      }, 1000)
    }
  }
});
