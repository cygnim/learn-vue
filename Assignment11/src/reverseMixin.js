export const reverseMixin = {
    data() {
        return {
            myText: 'barf'
        }
    },
    computed: {
        compText: function() {
            return this.myText.split("").reverse().join("")
        },
        compTextReversed: function() {
            return this.myText.split("").reverse().join("") + '(' + this.myText.length + ')'
        }
    },
    filters: {
        reverse: function(value) {
            return value.split("").reverse().join("")
        }
    }
}