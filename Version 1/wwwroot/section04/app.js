new Vue({
    el: '#app',
    data: {
        you: 100,
        monster: 100,
        showControls: false,
        logs: []
    },
    computed: {

    },
    methods: {
        startGame: function() {
            this.showControls = true;
        },
        attack: function() {
            let yourAttackValue = this.getRandom(1);
            let monsterAttackValue = this.getRandom(1);
            this.you -= monsterAttackValue;
            this.monster -= yourAttackValue;            
            this.addLog(false, monsterAttackValue);
            this.addLog(true, yourAttackValue);
        },
        specialAttack: function() {
            let yourAttackValue = this.getRandom(2);
            let monsterAttackValue = this.getRandom(1);
            this.you -= monsterAttackValue;
            this.monster -= yourAttackValue;            
            this.addLog(false, monsterAttackValue);
            this.addLog(true, yourAttackValue);
        },
        giveUp: function() {
            this.you = 100;
            this.monster = 100;
            this.showControls = false;
            this.log = []
        },
        heal: function() {
            let healValue = this.getRandom(1);
            let monsterAttackValue = this.getRandom(1);
            this.you += healValue;
            this.you -= monsterAttackValue;
            this.addLog(false, monsterAttackValue);
            this.logs.unshift({
                attackValue: 'You healed: ' + healValue,
                attacker: 'player-turn'
            });
        },
        addLog: function(isPlayer, value){            
            this.logs.unshift({
                attackValue: (isPlayer ? 'You' : 'Monster') + ' attacked: ' + value,
                attacker: (isPlayer ? 'player-turn' : 'monster-turn') 
            });
        },
        getRandom: function(multiplier) {
            return Math.floor(Math.random() * 10 * multiplier);
        }
    }
});